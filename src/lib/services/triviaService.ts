import trivias from '../../assets/data/trivias.json';

export const getTriviaById = (id: string) => {
  return trivias.find((trivia) => trivia.id === id);
};

export const getTrivias = () => {
  return trivias;
};
